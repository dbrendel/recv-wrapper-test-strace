CFLAGS += -Wall -Wextra

SRC = tst-recv.c
BIN = $(SRC:.c=)

.PHONY: all
all: $(BIN)

$(BIN): $(SRC)

.PHONY: check
check: $(BIN)
	 ./runtest.sh $(BIN)

.PHONY: clean
clean:
	$(RM) $(BIN)
