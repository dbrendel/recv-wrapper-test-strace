#!/bin/sh

# Copyright (c) 2023 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General
# Public License v.2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA

# Run a range of values for the recv() syscall wrapper and check with ltrace
# that recv() wrapper was called and check with strace that everything has been
# passed through to the syscall as expected.
#
# Not sure the fd (first argument) should also be permutated.

if test $# -lt 1; then
    echo "Usage: $0 <BINARY>"
    exit 0
fi

tst="$1"

x=0
while test $x -lt 100; do
    if test $x -eq 0; then
        hex=0
    else
        hex="0x$(printf "%x\n" $x)"
    fi

    ltrace_expected="$tst->recv(0xffffffff, IGNORE, 10, $x) = -1"
    ltrace_result="$(ltrace --align=0 -e recv ./$tst $hex 2>&1 | head -1 | \
                    sed 's/^\(.*ff, \)0x[0-9a-f]*\(,.*\)/\1IGNORE\2/')"

    if test "$ltrace_result" != "$ltrace_expected"; then
        printf "Mismatch: %s\n" "$ltrace_result"
        printf "Expected: %s\n" "$ltrace_expected"

        exit 1
    fi

    strace_expected="recvfrom(-1, IGNORE, 10, $hex, NULL, NULL) = -1 EBADF (Bad file descriptor)"
    strace_result="$(strace -X raw -e recvfrom ./$tst $hex 2>&1 | head -1 | \
                     sed 's/^\(.*-1, \)0x[0-9a-f]*\(,.*\)/\1IGNORE\2/')"

    if test "$strace_result" != "$strace_expected"; then
        printf "Mismatch: %s\n" "$strace_result"
        printf "Expected: %s\n" "$strace_expected"

        exit 1
    fi
    x=$((x+1))
done

echo "Success!"

exit 0
