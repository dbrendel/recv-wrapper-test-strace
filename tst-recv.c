/* Copyright (C) 2023 Dennis Brendel, Red Hat                                       
 *                                                                                  
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later   
 * version.                                                                         
 *                                                                                  
 * This program is distributed in the hope that it will be useful, but WITHOUT  
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more            
 * details.                                                                         
 *                                                                                  
 * You should have received a copy of the GNU General Public License along with 
 * this program. If not, see <https://www.gnu.org/licenses/>. */

#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>

int main (int argc, char **argv)
{
    char buf[10];
    ssize_t ret = 0;
    int flags = 0;

    if (argc < 2)
    {
        return EXIT_FAILURE;
    }

    flags = strtol(argv[1], NULL, 16);

    if (errno == ERANGE)
    {
        return EXIT_FAILURE;
    }

    ret = recv(-1, &buf, 10, flags);
    (void) ret;

    return EXIT_SUCCESS;
}
